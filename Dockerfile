FROM hashicorp/packer:1.6.4

LABEL \
  maintainer="Hauke Mettendorf <hauke.mettendorf@proum.de>" \
  org.opencontainers.image.title="dockerhub-description" \
  org.opencontainers.image.description="A simple alpine based container for building Hetzner Cloud snapshots" \
  org.opencontainers.image.authors="Hauke Mettendorf <hauke.mettendorf@proum.de>" \
  org.opencontainers.image.url="https://gitlab.com/proum-public/docker/packer" \
  org.opencontainers.image.vendor="https://proum.de" \
  org.opencontainers.image.licenses="GNUv2"

RUN apk --no-cache add \
    python3~3.8 \
    py3-pip~20 \
    && pip3 install --upgrade \
    'hcloud>=1.9' \
    'argparse>=1.4'
